import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class AppService {
    API_ENDPOINT = "http://51.15.233.87:9754";
    PROD_API_ENDPOINT = "https://frisha.co.ke/"; // https://frisha.co.ke/php/gen/file-upload.php
    constructor(
        private httpClient: HttpClient
    ) { }

    /**
     * getPackages
     */
    public getPackages(): Observable<any> {
        return this.httpClient.get(this.PROD_API_ENDPOINT + '/php/entities/soko-package-get-full.php');
    }

    public getItems(): Observable<any> {
        return this.httpClient.get(this.PROD_API_ENDPOINT + '/php/entities/soko-item-get-all.php');
    }

    public getOrders(): Observable<any> {
        return this.httpClient.get(this.PROD_API_ENDPOINT + '/php/entities/soko-order-get-full.php');
    }

    public purchase(cart: any): Observable<any> {
        return this.httpClient.post(this.PROD_API_ENDPOINT + '/php/entities/soko-order-create.php', cart);
    }

    // php
    public saveItem(item: any): Observable<any> {
        return this.httpClient.post(this.PROD_API_ENDPOINT + '/php/entities/soko-item-create.php', item);
    }

    // php
    public savePackage(pkg: any): Observable<any> {
        return this.httpClient.post(this.PROD_API_ENDPOINT + '/php/entities/soko-package-create.php', pkg);
    }

    public uploadFile(file: File): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('file', file);

        const req = new HttpRequest('POST', this.PROD_API_ENDPOINT + "php/gen/file-upload.php", formData, {
            reportProgress: false
        });

        return this.httpClient.request(req);
    }
}
