import { Component, OnInit } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';

import { AppService } from '../app.service'

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit {

  items = [];
  packages = [];
  orders = [];

  filteredPackages = [];
  filteredItems = [];

  selectedFile: File;
  uploadFileName = null;
  newPackage: any = {
    items: []
  };

  currentDisplay = "ORDER";

  // forms
  itemCreationForm = this.formBuilder.group({
    name: ['', [Validators.required]],
    description: ['', [Validators.required]],
    unit: ['pc', [Validators.required]],
    unitItems: [1, [Validators.required]],
    unitPrice: [null, [Validators.required]]
  })

  constructor(private formBuilder: FormBuilder, private appService: AppService) { }

  saveItem() {
    console.warn(this.itemCreationForm.value);


    let item = {
      name: this.itemCreationForm.value.name,
      description: this.itemCreationForm.value.description,
      unit: this.itemCreationForm.value.unit,
      unitItems: this.itemCreationForm.value.unitItems,
      unitPrice: this.itemCreationForm.value.unitPrice,
      imageUrl: this.uploadFileName
    }

    this.appService.saveItem(JSON.stringify(item)).subscribe(
      res => {
        console.log('saved item. Result: ', res);

        document.getElementById('cancelItemModalButton').click();
        // get items again
        this.getItems();
      }
    )
  }

  savePackage() {
    console.log('newPackage : ', this.newPackage);
    this.newPackage.imageUrl = this.uploadFileName;

    this.appService.savePackage(JSON.stringify(this.newPackage)).subscribe(
      res => {
        console.log('saved package. Result: ', res);

        document.getElementById('cancelPackageModalButton').click();
        // get items again
        this.getItems();
      }
    )
  }

  addItemToPackage() {
    this.newPackage.items.push({
      "itemId": null,
      "quantity": 1
    });
  }

  changePackageItem() {
    console.log('Changed package item');
    let totalPrice = 0;
    this.newPackage.items.forEach(packageItem => {
      // get matching item in items
      this.items.forEach(item => {
        if (item.id == packageItem.itemId) {
          packageItem.itemCost = item.unitPrice / item.unitItems * packageItem.quantity;
          packageItem.unit = item.unit;

          totalPrice += packageItem.itemCost;
        }
      })
    });
    this.newPackage.totalPrice = totalPrice;
  }

  changeFile(imageInput: any) {
    console.log('Received event', event);
    this.selectedFile = imageInput.files[0];
    console.log('File changed');
  }

  uploadFile() {
    if (this.selectedFile) {
      this.appService.uploadFile(this.selectedFile).subscribe(
        res => {
          if (res instanceof HttpResponse) {
            console.log('Upload done. File : ', res.body.fileName);
            this.uploadFileName = res.body.fileName;
          }
        }
      )
    } else {
      console.log('Nothing to upload');
    }
  }


  getItems() {
    this.appService.getItems().subscribe(
      res => {
        this.items = res;
        this.filteredItems = res;
        console.log('Received items', this.items);
      },
      err => {
        console.log('Error getting items', err);
      }
    )
  }

  getPackages() {
    this.appService.getPackages().subscribe(
      res => {
        this.packages = res;
        console.log('Received packages', this.packages);
        this.packages.forEach(pkg => {
          let totalCost = 0;
          pkg.items.forEach(item => {
            totalCost += item.totalPrice;
          });
          pkg.totalCost = totalCost;
        })
        this.filteredPackages = this.packages;
      },
      err => {
        console.log('Error getting packages', err);
      }
    )
  }

  getOrders() {
    this.appService.getOrders().subscribe(
      res => {
        this.orders = res;
        console.log('Received orders', this.orders);
      },
      err => {
        console.log('Error getting orders', err);
      }
    )
  }

  filterItems(event: any) {
    let keyword = event.target.value;
    console.log('keyword', keyword);


    let results = [];
    for (let item of this.items) {
      if (item.name && (item.name.toLowerCase().includes(keyword) || item.description && (item.description.toLowerCase().includes(keyword)))) {
        results.push(item);
      }
    }
    this.filteredItems = results;
  }

  filterPackages(event: any) {
    let keyword = event.target.value;
    console.log('keyword', keyword, 'packages', this.packages);


    let results = [];
    for (let pkg of this.packages) {
      if (pkg.name && (pkg.name.toLowerCase().includes(keyword.toLowerCase()) || (pkg.description && pkg.description.toLowerCase().includes(keyword.toLowerCase())))) {
        results.push(pkg);
      } else {
        let matched = false;
        for (let pkgItem of pkg.items) {
          if (pkgItem.name && (pkgItem.name.toLowerCase().includes(keyword.toLowerCase()) || (pkgItem.description && pkgItem.description.toLowerCase().includes(keyword.toLowerCase())))) {
            matched = true;
          }
        }

        if (matched) {
          results.push(pkg);
        }
      }
    }
    this.filteredPackages = results;
  }

  utilGetImageUrl(imageUrl: string) {
    return this.appService.PROD_API_ENDPOINT + "/uploads/" + imageUrl;
  }

  ngOnInit(): void {
    this.getItems();
    this.getPackages();
    this.getOrders();
  }

}
