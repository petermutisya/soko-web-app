import { Component, OnInit } from '@angular/core';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

import { AppService } from '../app.service'

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  stage = 0;
  cart: any = {};

  private CART_STORAGE_KEY = "cart";

  constructor(
    private appService: AppService,
    @Inject(LOCAL_STORAGE) private localStorage: StorageService,
  ) { }

  purchase() {
    // validate
    if (this.cart.deliveryDetails && this.cart.deliveryDetails.name && this.cart.deliveryDetails.msisdn && this.cart.deliveryDetails.address && this.cart.deliveryDetails.date) {
      this.appService.purchase(JSON.stringify(this.cart)).subscribe(
        result => {
          console.log('SUccessfully made purchase', result);
          this.stage = 1;
          this.cart = {};
          this.localStorage.remove(this.CART_STORAGE_KEY);
        },
        error => {
          console.error('Encountered error', error);
        }
      )
    }
  }

  ngOnInit(): void {
    if (this.localStorage.get(this.CART_STORAGE_KEY)) {
      this.cart = this.localStorage.get(this.CART_STORAGE_KEY);

      if (!this.cart.deliveryDetails) {
        let currentDate = new Date();
        let today = currentDate.getFullYear() + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + ("0" + currentDate.getDate() + 1).slice(-2);


        this.cart.deliveryDetails = {
          time: "08:00",
          date: today
        };
      }

      console.log('cart', this.cart);
    }
  }

}
