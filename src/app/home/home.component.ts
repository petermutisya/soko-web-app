import { Component, OnInit } from '@angular/core';
import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AppService } from '../app.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private appService: AppService,
    @Inject(LOCAL_STORAGE) private localStorage: StorageService,
  ) { }
  packages = [];
  filteredPackages = [];
  items = [];
  filteredItems = [];
  private CART_STORAGE_KEY = "cart";
  cart = {
    items: [],
    totalPrice: 1200,
    packageId: null,
    packageName: null
  };

  title = 'soko-web-app';

  filterItems(event: any) {
    let keyword = event.target.value;
    console.log('keyword', keyword);


    let results = [];
    for (let item of this.items) {
      if (item.name !== null && (item.name.toLowerCase().includes(keyword) || item.description.toLowerCase().includes(keyword))) {
        results.push(item);
      }
    }
    this.filteredItems = results;
  }

  filterPackages(event: any) {
    let keyword = event.target.value;
    console.log('keyword', keyword, 'packages', this.packages);


    let results = [];
    for (let pkg of this.packages) {
      if (pkg.name !== null && (pkg.name.toLowerCase().includes(keyword.toLowerCase()) || pkg.description.toLowerCase().includes(keyword.toLowerCase()))) {
        results.push(pkg);
      } else {
        let matched = false;
        for (let pkgItem of pkg.items) {
          if (pkgItem.name !== null && (pkgItem.name.toLowerCase().includes(keyword.toLowerCase()) || pkgItem.description.toLowerCase().includes(keyword.toLowerCase()))) {
            matched = true;
          }
        }

        if (matched) {
          results.push(pkg);
        }
      }
    }
    this.filteredPackages = results;
  }

  addPackageToCart(id: number) {
    console.log('Request to add pkg to cart', id)
    // get package by id
    for (let pkg of this.packages) {
      if (pkg.id === id) {
        this.cart.packageId = pkg.id;
        this.cart.packageName = pkg.name;

        for (let item of pkg.items) {
          this.addItemToCart(item.id, item.quantity);
          console.log('updated cart:', this.cart.items);
        }
        this.reCalculateCartPrice();
        return;
      }
    }
  }

  emptyCart() {
    this.cart = {
      items: [],
      totalPrice: 0,
      packageId: null,
      packageName: null
    };
    this.localStorage.set(this.CART_STORAGE_KEY, this.cart);
  }

  addItemToCart(id: number, quantity?: number) {
    console.log('request to add item to cart', id, quantity);
    if (!quantity) {
      quantity = 1;
    }

    for (let item of this.items) {
      if (item.id === id) {
        console.log('adding item', item);
        // check if it exists in cart and increment qnty
        for (let cartItem of this.cart.items) {
          if (cartItem.id === item.id) {
            cartItem.quantity = +cartItem.quantity + +quantity;
            this.reCalculateCartPrice();
            return;
          }
        }

        let clonedItem = JSON.parse(JSON.stringify(item));
        clonedItem.quantity = quantity;

        this.cart.items.push(clonedItem);

        console.log('this.cart.items', this.cart.items);
        this.reCalculateCartPrice();
        return;
      }
    }
  }

  reCalculateCartPrice() {
    // calculate total price for each item
    let cartTotalPrice = 0;
    let itemCount = 0;

    this.cart.items = this.cart.items.filter(function (item) {
      return item.quantity > 0;
    });

    for (let item of this.cart.items) {
      item.totalPrice = Math.round(item.unitPrice * item.quantity / item.unitItems);

      cartTotalPrice += item.totalPrice;

      itemCount += item.quantity;
    }
    this.cart.totalPrice = cartTotalPrice;
    this.localStorage.set(this.CART_STORAGE_KEY, this.cart);
  }

  ngOnInit(): void {
    this.appService.getPackages().subscribe(
      result => {
        this.packages = result;
        this.filteredPackages = result;
      },
      err => {
        console.error('Error', err);

      }
    )

    this.appService.getItems().subscribe(
      result => {
        this.items = result;
        this.filteredItems = result;
      },
      err => {
        console.error('Error', err);

      })

    if (this.localStorage.get(this.CART_STORAGE_KEY)) {
      this.cart = this.localStorage.get(this.CART_STORAGE_KEY);
    }
  }

  utilGetImageUrl(imageUrl: string) {
    return this.appService.PROD_API_ENDPOINT + "/uploads/" + imageUrl;
  }


}
